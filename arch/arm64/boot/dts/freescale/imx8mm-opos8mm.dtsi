// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
//
// Copyright 2021 Armadeus Systems - <support@armadeus.com>

#include "imx8mm.dtsi"

/ {
	aliases {
		rtc0 = &pcf85063a;
		rtc1 = &snvs_rtc;
	};

	memory@40000000 {
		reg = <0x00000000 0x4000000 0 0>; /* will be filled by U-Boot */
		device_type = "memory";
	};

	usdhc3_pwrseq: usdhc3-pwrseq {
		compatible = "mmc-pwrseq-simple";
		reset-gpios = <&gpio3 15 GPIO_ACTIVE_LOW>;
	};
};

&A53_0 {
	cpu-supply = <&reg_vdd_arm_0v9>;
};

&A53_1 {
	cpu-supply = <&reg_vdd_arm_0v9>;
};

&A53_2 {
	cpu-supply = <&reg_vdd_arm_0v9>;
};

&A53_3 {
	cpu-supply = <&reg_vdd_arm_0v9>;
};

&fec1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_fec1>;
	phy-mode = "rgmii-id";
	phy-handle = <&ethphy0>;
	phy-reset-gpios = <&gpio2 20 GPIO_ACTIVE_LOW>;
	phy-reset-duration = <10>;
	fsl,magic-packet;
	status = "okay";

	mdio {
		#address-cells = <1>;
		#size-cells = <0>;

		ethphy0: ethernet-phy@1 {
			compatible = "ethernet-phy-ieee802.3-c22";
			reg = <1>;
			interrupt-parent = <&gpio2>;
			interrupts = <19 IRQ_TYPE_LEVEL_LOW>;
			status = "okay";
		};
	};
};

/* Bluetooth */
&uart2 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_uart2>;
	uart-has-rtscts;
	status = "okay";
};

/* eMMC */
&usdhc1 {
	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc1>;
	pinctrl-1 = <&pinctrl_usdhc1_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc1_200mhz>;
	bus-width = <8>;
	non-removable;
	vmmc-supply = <&reg_vdd_3v3>;
	vqmmc-supply = <&reg_vdd_1v8>;
	mmc-ddr-1_8v;
	mmc-hs200-1_8v;
	mmc-hs400-1_8v;
	mmc-hs400-enhanced-strobe;
	status = "okay";
};

/* WiFi */
&usdhc3 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_usdhc3>;
	bus-width = <4>;
	no-1-8-v;
	non-removable;
	mmc-pwrseq = <&usdhc3_pwrseq>;
	status = "okay";

	#address-cells = <1>;
	#size-cells = <0>;

	brcmf: wifi@1 {
		compatible = "brcm,bcm4329-fmac";
		reg = <1>;
		interrupt-parent = <&gpio3>;
		interrupts = <5 IRQ_TYPE_LEVEL_LOW>;
		interrupt-names = "host-wake";
	};
};

&i2c1 {
	clock-frequency = <100000>;
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_i2c1>;
	status = "okay";

	pmic: pmic@25 {
		compatible = "nxp,pca9450a";
		reg = <0x25>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_pmic>;
		interrupt-parent = <&gpio1>;
		interrupts = <3 IRQ_TYPE_LEVEL_LOW>;

		regulators {
			reg_vdd_soc_0v8: BUCK1 {
				regulator-name = "BUCK1";
				regulator-min-microvolt = <875000>;
				regulator-max-microvolt = <875000>;
				regulator-boot-on;
				regulator-always-on;
				regulator-ramp-delay = <3125>;
			};

			reg_vdd_arm_0v9: BUCK2 {
				regulator-name = "BUCK2";
				regulator-min-microvolt = <850000>;
				regulator-max-microvolt = <1025000>;
				regulator-boot-on;
				regulator-always-on;
				regulator-ramp-delay = <3125>;
				nxp,dvs-run-voltage = <975000>;
				nxp,dvs-standby-voltage = <850000>;
			};

			reg_vdd_dram_pu_0v9: BUCK3 {
				regulator-name = "BUCK3";
				regulator-min-microvolt = <975000>;
				regulator-max-microvolt = <975000>;
				regulator-boot-on;
				regulator-always-on;
			};

			reg_vdd_3v3: BUCK4 {
				regulator-name = "BUCK4";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;
				regulator-boot-on;
				regulator-always-on;
			};

			reg_vdd_1v8: BUCK5 {
				regulator-name = "BUCK5";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-boot-on;
				regulator-always-on;
			};

			reg_nvcc_dram_1v1: BUCK6 {
				regulator-name = "BUCK6";
				regulator-min-microvolt = <1125000>;
				regulator-max-microvolt = <1125000>;
				regulator-boot-on;
				regulator-always-on;
			};

			reg_nvcc_snvs_1v8: LDO1 {
				regulator-name = "LDO1";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-boot-on;
				regulator-always-on;
			};

			reg_vdd_snvs_0v8: LDO2 {
				regulator-name = "LDO2";
				regulator-min-microvolt = <850000>;
				regulator-max-microvolt = <850000>;
				regulator-boot-on;
				regulator-always-on;
			};

			reg_vdda_1v8: LDO3 {
				regulator-name = "LDO3";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
				regulator-boot-on;
				regulator-always-on;
			};

			reg_vdd_phy_0v9: LDO4 {
				regulator-name = "LDO4";
				regulator-min-microvolt = <900000>;
				regulator-max-microvolt = <900000>;
				regulator-boot-on;
				regulator-always-on;
			};
		};
	};

	pcf85063a: rtc@51 {
		compatible = "nxp,pcf85063a";
		reg = <0x51>;
		pinctrl-names = "default";
		pinctrl-0 = <&pinctrl_rtc>;
		interrupt-parent = <&gpio3>;
		interrupts = <2 IRQ_TYPE_LEVEL_LOW>;
	};
};

&wdog1 {
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_wdog>;
	fsl,ext-reset-output;
	status = "okay";
};

&iomuxc {
	pinctrl_fec1: fec1grp {
		fsl,pins = <
			MX8MM_IOMUXC_ENET_MDC_ENET1_MDC			0x92
			MX8MM_IOMUXC_ENET_MDIO_ENET1_MDIO		0x92
			MX8MM_IOMUXC_ENET_RD0_ENET1_RGMII_RD0		0x90
			MX8MM_IOMUXC_ENET_RD1_ENET1_RGMII_RD1		0x90
			MX8MM_IOMUXC_ENET_RD2_ENET1_RGMII_RD2		0x90
			MX8MM_IOMUXC_ENET_RD3_ENET1_RGMII_RD3		0x90
			MX8MM_IOMUXC_ENET_RXC_ENET1_RGMII_RXC		0x90
			MX8MM_IOMUXC_ENET_RX_CTL_ENET1_RGMII_RX_CTL	0x90
			MX8MM_IOMUXC_ENET_TD0_ENET1_RGMII_TD0		0x12
			MX8MM_IOMUXC_ENET_TD1_ENET1_RGMII_TD1		0x12
			MX8MM_IOMUXC_ENET_TD2_ENET1_RGMII_TD2		0x12
			MX8MM_IOMUXC_ENET_TD3_ENET1_RGMII_TD3		0x12
			MX8MM_IOMUXC_ENET_TXC_ENET1_RGMII_TXC		0x12
			MX8MM_IOMUXC_ENET_TX_CTL_ENET1_RGMII_TX_CTL	0x12
			/* ETH1_RESET# */
			MX8MM_IOMUXC_SD2_WP_GPIO2_IO20			0x92
			/* ETH1_INT */
			MX8MM_IOMUXC_SD2_RESET_B_GPIO2_IO19		0x92
		>;
	};

	pinctrl_i2c1: i2c1grp {
		fsl,pins = <
			MX8MM_IOMUXC_I2C1_SCL_I2C1_SCL			0x40000082
			MX8MM_IOMUXC_I2C1_SDA_I2C1_SDA			0x40000082
		>;
	};

	pinctrl_pmic: pmicgrp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO03_GPIO1_IO3		0x90
		>;
	};

	pinctrl_uart2: uart2grp {
		fsl,pins = <
			MX8MM_IOMUXC_SAI3_RXC_UART2_DCE_CTS_B	0x90
			MX8MM_IOMUXC_SAI3_RXD_UART2_DCE_RTS_B	0x90
			MX8MM_IOMUXC_SAI3_TXC_UART2_DCE_TX	0x90
			MX8MM_IOMUXC_SAI3_TXFS_UART2_DCE_RX	0x90
			/* BT_EN */
			MX8MM_IOMUXC_NAND_READY_B_GPIO3_IO16	0x10
		>;
	};

	pinctrl_usdhc1: usdhc1grp {
		fsl,pins = <
			MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK			0x190
			MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD			0x1d0
			MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0		0x1d0
			MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1		0x1d0
			MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2		0x1d0
			MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3		0x1d0
			MX8MM_IOMUXC_SD1_DATA4_USDHC1_DATA4		0x1d0
			MX8MM_IOMUXC_SD1_DATA5_USDHC1_DATA5		0x1d0
			MX8MM_IOMUXC_SD1_DATA6_USDHC1_DATA6		0x1d0
			MX8MM_IOMUXC_SD1_DATA7_USDHC1_DATA7		0x1d0
			MX8MM_IOMUXC_SD1_RESET_B_USDHC1_RESET_B 	0x1d0
			MX8MM_IOMUXC_SD1_STROBE_USDHC1_STROBE		0x190
		>;
	};

	pinctrl_usdhc1_100mhz: usdhc1grp100mhz {
		fsl,pins = <
			MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK			0x194
			MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD			0x1d4
			MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0		0x1d4
			MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1		0x1d4
			MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2		0x1d4
			MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3		0x1d4
			MX8MM_IOMUXC_SD1_DATA4_USDHC1_DATA4		0x1d4
			MX8MM_IOMUXC_SD1_DATA5_USDHC1_DATA5		0x1d4
			MX8MM_IOMUXC_SD1_DATA6_USDHC1_DATA6		0x1d4
			MX8MM_IOMUXC_SD1_DATA7_USDHC1_DATA7		0x1d4
			MX8MM_IOMUXC_SD1_RESET_B_USDHC1_RESET_B 	0x1d4
			MX8MM_IOMUXC_SD1_STROBE_USDHC1_STROBE		0x194
		>;
	};

	pinctrl_usdhc1_200mhz: usdhc1grp200mhz {
		fsl,pins = <
			MX8MM_IOMUXC_SD1_CLK_USDHC1_CLK			0x196
			MX8MM_IOMUXC_SD1_CMD_USDHC1_CMD			0x1d6
			MX8MM_IOMUXC_SD1_DATA0_USDHC1_DATA0		0x1d6
			MX8MM_IOMUXC_SD1_DATA1_USDHC1_DATA1		0x1d6
			MX8MM_IOMUXC_SD1_DATA2_USDHC1_DATA2		0x1d6
			MX8MM_IOMUXC_SD1_DATA3_USDHC1_DATA3		0x1d6
			MX8MM_IOMUXC_SD1_DATA4_USDHC1_DATA4		0x1d6
			MX8MM_IOMUXC_SD1_DATA5_USDHC1_DATA5		0x1d6
			MX8MM_IOMUXC_SD1_DATA6_USDHC1_DATA6		0x1d6
			MX8MM_IOMUXC_SD1_DATA7_USDHC1_DATA7		0x1d6
			MX8MM_IOMUXC_SD1_RESET_B_USDHC1_RESET_B 	0x1d6
			MX8MM_IOMUXC_SD1_STROBE_USDHC1_STROBE		0x196
		>;
	};

	pinctrl_usdhc3: usdhc3grp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_WE_B_USDHC3_CLK		0x92
			MX8MM_IOMUXC_NAND_WP_B_USDHC3_CMD		0x92
			MX8MM_IOMUXC_NAND_DATA04_USDHC3_DATA0		0x92
			MX8MM_IOMUXC_NAND_DATA05_USDHC3_DATA1		0x92
			MX8MM_IOMUXC_NAND_DATA06_USDHC3_DATA2		0x92
			MX8MM_IOMUXC_NAND_DATA07_USDHC3_DATA3		0x92
			/* WLAN_EN */
			MX8MM_IOMUXC_NAND_RE_B_GPIO3_IO15		0x92
			/* WLAN_GPIO0 */
			MX8MM_IOMUXC_NAND_CLE_GPIO3_IO5			0x90
		>;
	};

	pinctrl_rtc: rtcgrp {
		fsl,pins = <
			MX8MM_IOMUXC_NAND_CE1_B_GPIO3_IO2		0x90
		>;
	};

	pinctrl_wdog: wdoggrp {
		fsl,pins = <
			MX8MM_IOMUXC_GPIO1_IO02_WDOG1_WDOG_B		0x92
		>;
	};
};
